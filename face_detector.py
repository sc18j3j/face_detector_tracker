import os
import cv2
import numpy as np

class FaceDetector:
    def __init__(self, confidence = 0.5):
        self.net = cv2.dnn.readNetFromCaffe(    \
            os.path.dirname(os.path.realpath(__file__)) + '/weights/OpenCV/deploy.prototxt',   \
            os.path.dirname(os.path.realpath(__file__)) + '/weights/OpenCV/res10_300x300_ssd_iter_140000_fp16.caffemodel')
        self.confidence = confidence

    def getFaces(self, frame):
        # get image and preprocess it
        frame_height, frame_width = frame.shape[:2]
        image = cv2.resize(frame, (300,300))
        blob = cv2.dnn.blobFromImage(image, 1, (300,300), (104, 177, 123))
        # send it thru the dnn
        self.net.setInput(blob)
        net_output = self.net.forward()
        # output array
        detections = []
        frame_bb = np.copy(frame)
        # for each detection
        for i in range(net_output.shape[2]):
            confidence = net_output[0,0,i,2]
            if confidence > self.confidence:
                box = net_output[0,0,i,3:7]
                # scale bounding box and put it on the frame
                x1 = int(box[0] * frame_width)
                y1 = int(box[1] * frame_height)
                x2 = int(box[2] * frame_width)
                y2 = int(box[3] * frame_height)
                w = int(x2 - x1)
                h = int(y2 - y1)
                detections.append([confidence, x1, y1, w, h])
                cv2.putText(frame_bb, 'face: {:.3f}'.format(confidence), (x1, y1 - 5), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0,255,0), 1)
                cv2.rectangle(frame_bb, (x1, y1), (x2, y2), (0,255,0), 2)
        # display the frame
        return detections, frame_bb