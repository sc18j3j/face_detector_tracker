# REFERENCES
# ----------
# PJ Reddie, YOLOv3
# available at: https://pjreddie.com/darknet/yolo
# ----------
# Adrian Rosenbrock, YOLOv3 OpenCV tutorial
# available at: https://www.pyimagesearch.com/
# ----------

import numpy as np
import networkx as nx
import cv2
from threading import Thread

weights_dir = '/tiago_ws/src/lasr_object_detection_yolo/models/coco/'

class DetectedObject:
    def __init__(self, label, box):
        self.label = label
        self.box = box

class TrackedObject:
    def __init__(self, id, label, box, frame):
        self.id = id
        self.label = label
        csrt = cv2.TrackerCSRT_create()
        csrt.init(frame, box)
        self.tracker = csrt
        self.box = box
        self.undetected_frames = 0

    def reinit_tracker(self, frame, box):
        csrt = cv2.TrackerCSRT_create()
        csrt.init(frame, box)
        self.tracker = csrt

    def update(self, frame):
        (success, box) = self.tracker.update(frame)
        if success:
            self.box = box
        else:
            self.box = (0, 0, 0, 0)

# Intersection over Union of two bounding boxes
def calcBoundingBoxIOU(a, b):
    # get intersection coords
    xleft = max(a[0], b[0])
    xright = min(a[0] + a[2], b[0] + b[2])
    ytop = max(a[1], b[1])
    ybottom = min(a[1] + a[3], b[1] + b[3])
    # calculate intersection area
    width = max(0, xright - xleft)
    height = max(0, ybottom - ytop)
    intersection = float(width * height)
    # calculate union area
    union = float((a[2] * a[3]) + (b[2] * b[3]) - intersection)
    # return IOU
    if union == 0:
        return 0
    return intersection/union

class DetectorTracker:
    def __init__(self, yolo_confidence = 0.5, yolo_nms = 0.3, object_list = ('person',), redetect_rate = 15, layoff = 1):
        # yolo params
        self.yolo_confidence = yolo_confidence
        self.yolo_nms = yolo_nms
        # tracker params
        self.object_list = object_list
        self.redetect_rate = redetect_rate
        # yolo net setup
        self.labels = open(weights_dir + "classes.txt").read().strip().split("\n")
        self.weights_path = weights_dir + "yolov3.weights"
        self.config_path = weights_dir + "yolov3.cfg"
        self.net = cv2.dnn.readNetFromDarknet(self.config_path, self.weights_path)
        # yolo layers
        layers = self.net.getLayerNames()
        self.yolo_layers = []
        for x in self.net.getUnconnectedOutLayers():
            layer_index = x[0] - 1
            self.yolo_layers.append(layers[layer_index])
        # tracker setup
        self.trackers = []
        self.iteration = redetect_rate
        self.layoff = layoff
        self.detecting = False
        self.current_id = 0

    def createTracker(self, object_id, label, box, frame):
        return TrackedObject(object_id, label, box, frame)

    # OBJECT DETECTION
    def detectObjects(self, frame):
        # get frame dims
        (frame_height, frame_width) = frame.shape[:2]
        # preprocess image and get output of yolo layers thru DNN
        blob = cv2.dnn.blobFromImage(frame, 1/255.0, (416,416), swapRB=True, crop=False)
        self.net.setInput(blob)
        yolo_outputs = self.net.forward(self.yolo_layers)
        # array setup
        boxes = []
        confidences = []
        class_ids = []
        # loop over detections
        for output in yolo_outputs:
            for detection in output:
                predictions = detection[5:]
                class_id = np.argmax(predictions)
                confidence = predictions[class_id]
                # if the detection meets the confidence threshold
                if confidence > self.yolo_confidence:
                    # get bounding box
                    centre_x = detection[0] * frame_width
                    centre_y = detection[1] * frame_height
                    w = int(detection[2] * frame_width)
                    h = int(detection[3] * frame_height)
                    x = int(centre_x - (w/2))
                    y = int(centre_y - (h/2))
                    # update boxes, confidences and classIDs with detection
                    boxes.append([x, y, w, h])
                    confidences.append(float(confidence))
                    class_ids.append(class_id)
        # apply NMS
        kept_indices = cv2.dnn.NMSBoxes(boxes, confidences, self.yolo_confidence, self.yolo_nms)
        detected_objects = []
        # for any kept indices after NMS
        if len(kept_indices) > 0:
            for i in kept_indices.flatten():
                label = self.labels[class_ids[i]]
                # extract the bounding box coordinates
                box = tuple(boxes[i])
                # append detected object of interest to array
                if label in self.object_list:
                    detected_objects.append(DetectedObject(label, box))
        return detected_objects

    # MAX-MATCHING
    # Max-match objects on bounding-box IOUs
    def maxMatchObjects(self, detected_objects, frame):
        G = nx.Graph()
        redetected_objects = []
        matched_objects = []
        # if there is at least one tracker-detection pair
        if len(detected_objects) > 0 and len(self.trackers) > 0:
            # create weighted graph IOU-like, where labels are the same
            for t_index in range(len(self.trackers)):
                for d_index in range(len(detected_objects)):
                    detection = detected_objects[d_index]
                    tracker = self.trackers[t_index]
                    if detection.label == tracker.label:
                        G.add_edge(('t', t_index), ('d' , d_index), weight = calcBoundingBoxIOU(detection.box, tracker.box))
                    else:
                        G.add_edge(('t', t_index), ('d' , d_index), weight = 0)
            # use max weight matching (blossom algorithm)
            max_match = nx.max_weight_matching(G)
            # update trackers for matches
            for match in max_match:
                # get indices
                for node in match:
                    if 't' in node:
                        t_index = node[1]
                    else:
                        d_index = node[1]
                # given a non-zero IOU edge
                if not G[('t', t_index)][('d', d_index)]['weight'] == 0:
                    # indicate matched detection
                    detection = detected_objects[d_index]
                    tracker = self.trackers[t_index]
                    matched_objects.append(detection)
                    # reinitialise tracker
                    tracker.reinit_tracker(frame, detection.box)
                    tracker.undetected_frames = -1
        # only keep trackers undetected for less than or equal to layoff
        for tracker in self.trackers:
            tracker.undetected_frames += 1
            if tracker.undetected_frames <= self.layoff:
                redetected_objects.append(tracker)

        self.trackers = redetected_objects
        return matched_objects

    # UPDATE DETECTOR TRACKER
    def update(self, frame):
        # update existing trackers for new bounding boxes
        # use threading for extra hayai!!! bonus
        jobs = []
        for tracker in self.trackers:
            update_thread = Thread(target = tracker.update, args = (frame,))
            update_thread.start()
            jobs.append(update_thread)
        # check all jobs are complete before continuing
        for job in jobs:
            job.join()

        # on "detection" iteration
        if self.iteration == self.redetect_rate:
            self.iteration = 0
            self.detecting = True # detection mode flag
            # detect objects for tracking
            detected_objects = self.detectObjects(frame)
            # match detections to existing trackers
            matched_objects =  self.maxMatchObjects(detected_objects, frame)
            # create trackers for newly detected objects
            for detection in detected_objects:
                if detection not in matched_objects:
                    init_tracker = self.createTracker(self.current_id, detection.label, detection.box, frame)
                    self.trackers.append(init_tracker)     
                    self.current_id += 1

        else:
            self.detecting = False # detection mode flag
        
        # iteration up
        self.iteration += 1