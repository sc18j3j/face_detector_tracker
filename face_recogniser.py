import os
import cv2
import pickle
import numpy as np
from sklearn import svm
from sklearn.exceptions import NotFittedError

class FaceRecogniser:
    def __init__(self, face_recog_confidence = 0.7):
        self.face_recog_confidence = face_recog_confidence
        self.net = cv2.dnn.readNetFromTorch(os.path.dirname(os.path.realpath(__file__)) + '/weights/OpenFace/nn4.small2.v1.t7')
        self.clf = svm.SVC(gamma='scale', probability=True)

        self.db_path = os.path.dirname(os.path.realpath(__file__)) + '/face_database/'
        self.embeddings = []
        self.names = []
        self.unique_names = []

        self.load_from_database(target='unknown')


    def getEmbedding(self, image, bounding_box=None):
        if bounding_box is not None:
            (x, y, w, h) = bounding_box
            image = image[y : y + h, x : x + w]
        blob = cv2.dnn.blobFromImage(image, 1.0/255, (96,96), (0,0,0), swapRB=True, crop=False)
        self.net.setInput(blob)
        return self.net.forward()


    def recogniseFaceFromEmbedding(self, embedding):
        try:
            # get predictions and select the maximum
            predictions = self.clf.predict_proba(embedding)[0]
            max_index = np.argmax(predictions)
            confidence = predictions[max_index]
            person = self.clf.classes_[max_index]

            # return person and confidence if threshold is met
            if confidence >= self.face_recog_confidence:
                return person, confidence
        
        except NotFittedError as e:
            print 'Warning: classifier has not been trained'

        if len(self.unique_names) == 1:
            return self.names[0], 100
        
        return None, 0
    
    def get_classnames(self):
        return self.unique_names
    
    def load_from_database(self, target=None):
        for root, dirs, files in os.walk(self.db_path):
            # for each person in the image folder
            for name in dirs:
                if not name in self.unique_names and (target is None or name==target):
                    person_path = self.db_path + name + '/'
                    # for all feature vectors of the person
                    for root, dirs, files in os.walk(person_path):
                        for file_name in files:
                            suffix = file_name[-4:]
                            if suffix == '.npy':
                                # get embeddings from file and store embeddings, name
                                embedding = np.load(person_path + file_name)
                                self.embeddings.append(embedding)
                                self.names.append(name)
        
        # update unique names list
        self.unique_names = np.unique(self.names)


    def save_embeddings_to_database(self, embeddings, name):
        # create embeddings folder for person
        person_path = self.db_path + name + '/'
        if not os.path.exists(self.db_path):
            os.makedirs(self.db_path)
        if not os.path.exists(person_path):
            os.makedirs(person_path)

            # create index file for no. of embeddings
            index_file_path = person_path + '.current_index'
            if not os.path.exists(index_file_path):
                current_index = 0
                with open(index_file_path, 'a') as f:
                    f.write(str(current_index))

            # save each embedding as a .npy file
            for embedding in embeddings:
                embedding = self.face_recogniser.getEmbedding(face)[0]
                file_name = person_path + name + str(current_index) + '.npy'
                np.save(file_name, embedding)

                print file_name
                current_index += 1

            # store current index       
            with open(index_file_path, 'w') as f:
                f.write(str(current_index))
        else:
            print 'person already exists!'


    def add_embeddings_to_temp(self, embeddings, name):
        if not name in self.unique_names:
            for embedding in embeddings:
                self.embeddings.append(embedding)
                self.names.append(name)
    
        # update unique names list
        self.unique_names = np.unique(self.names)

    def load_pretrained_classifier(self):
        self.clf = pickle.load(open(os.path.dirname(os.path.realpath(__file__)) + '/weights/SVM/face_classifier.pkl', 'rb'))
    
    def save_pretrained_classifier(self):
        weights_path = os.path.dirname(os.path.realpath(__file__)) + '/weights/SVM/'

        # create SVM dir if it does not exist
        if not os.path.exists(weights_path):
            os.makedirs(weights_path)

        # write pcikled SVM to file
        with open(weights_path + 'face_classifier.pkl', 'w') as f:
            f.write(pickle.dumps(self.clf))
        
        print 'face_classifier.pkl written to file'


    def train_classifier(self):
        if len(np.unique(self.names)) > 1:
            print 'training face_classifier SVM'
            self.clf.fit(self.embeddings,self.names)
        else:
            print 'Warning: number of classes has to be greater than 1 to train face classifier'