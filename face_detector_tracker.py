import os

import numpy as np
import math
import networkx as nx
from detector_tracker import TrackedObject, DetectorTracker, calcBoundingBoxIOU
from face_detector import FaceDetector
from face_recogniser import FaceRecogniser

class NamedTrackedObject(TrackedObject):
    def __init__(self, id, label, box, frame, name = None):
        TrackedObject.__init__(self, id, label, box, frame)
        self.name = name
        self.has_face = False

def calcBoundingBoxIOUOverDistance(a, b):
    a_x = a[0] + a[2]/2
    b_x = b[0] + b[2]/2
    euclidian_dist = int(math.sqrt(pow(a_x - b_x, 2) + pow(a[1] - b[1], 2)))
    if euclidian_dist == 0:
        return calcBoundingBoxIOU(a,b)
    return calcBoundingBoxIOU(a,b) / euclidian_dist

class FaceDetectorTracker(DetectorTracker):
    def __init__(self, yolo_confidence = 0.5, yolo_nms = 0.3,
                    object_list = ('person',), redetect_rate = 15, layoff = 1, \
                    face_detect_confidence = 0.5, face_recog_confidence = 0.7):
        DetectorTracker.__init__(self, yolo_confidence, yolo_nms, object_list, redetect_rate, layoff)
        self.face_detector = FaceDetector(confidence = face_detect_confidence)
        self.face_recogniser = FaceRecogniser(face_recog_confidence)
    
    def createTracker(self, object_id, label, box, frame, name = ''):
        return NamedTrackedObject(object_id, label, box, frame, name)

    def maxMatchFaces(self, frame, faces):
        G = nx.Graph()
        if len(self.trackers) > 0 and len(faces) > 0:
            for t_index in range(len(self.trackers)):
                # set tracker has face property to false
                tracker = self.trackers[t_index]
                # for each tracker-face pair
                for f_index in range(len(faces)):
                    # add an edge between them of weight bounding-box IOU/euclidian distance
                    face = faces[f_index]
                    if tracker.label == 'person':
                        G.add_edge(('t', t_index), ('f', f_index), weight = calcBoundingBoxIOUOverDistance(tracker.box, face[1:5]))
                    else:
                        G.add_edge(('t', t_index), ('f', f_index), weight = 0)
            # use max weight matching (blossom algorithm)
            max_match = nx.max_weight_matching(G)
            # update trackers for matches
            for match in max_match:
                # get indices
                for node in match:
                    if 't' in node:
                        t_index = node[1]
                    else:
                        f_index = node[1]
                # given a non-zero IOU edge, update name field of tracker
                if not G[('t', t_index)][('f', f_index)]['weight'] == 0:
                    self.trackers[t_index].has_face = True
                    # only change the name if previously unknown
                    t_name = self.trackers[t_index].name
                    if t_name == '' or t_name == 'unknown':
                        # get face embeddings and predict identity of face
                        embedding = self.face_recogniser.getEmbedding(frame, faces[f_index][1:5])
                        recognised_face = self.face_recogniser.recogniseFaceFromEmbedding(embedding)
                        if recognised_face is not None:
                            self.trackers[t_index].name = recognised_face[0]

    def update(self, frame):
        DetectorTracker.update(self, frame)
        if self.detecting:
            for tracker in self.trackers:
                tracker.has_face = False
            # perform face detection
            detected_faces, frame_bb = self.face_detector.getFaces(frame)
            # max-weight matching on IOU/distance for each face to person bounding box
            self.maxMatchFaces(frame, detected_faces)        